{
  outputs = {
    ...
  } @ inputs: {
    templateHelm = {
      name,
      helmHash,
      pkgs,
      chart ? null,
      repo ? null,
      version ? null,
      src ? null,
      values ? null,
      kubeVersion ? null,
      namespace ? "default",
      extraArgs ? "",
    }: let
      isOci = str: (builtins.substring 0 6 str) == "oci://";

      valuesFile = if values != null then (pkgs.writeTextFile {
        name = "helm-values-${name}";
        text = pkgs.lib.generators.toYAML {} values;
      }) else null;
    in pkgs.stdenv.mkDerivation {
      inherit src;
      dontUnpack = src == null;
      name = "helm-template-${name}";
      buildInputs = [pkgs.kubernetes-helm];
      doConfigure = !(chart != null && isOci chart);
      # helm SUCKS https://github.com/helm/helm/issues/8036
      configurePhase = if chart == null then ''
        runHook preConfigure
        HOME=$TMPDIR
        i=0;
        if [ -f "./Chart.lock" ]; then
          cat ./Chart.lock | grep repository | awk '{print $2}' | while read -r line ; do
            i=$((i+1)); helm repo add fuckhelm$i $line;
          done;
        fi
        echo "Added charts, building..."
        helm dep build
        runHook postConfigure
      '' else if repo != null then ''
        runHook preConfigure
        HOME=$TMPDIR
        helm repo add ${builtins.elemAt (pkgs.lib.splitString "/" chart) 0} ${repo}
        runHook postConfigure
      '' else '''';
      buildPhase = ''
        runHook preBuild
        HOME=$TMPDIR
        mkdir -p $out
        helm template ${name} ${if (repo == null && !(isOci chart)) then "." else chart} ${if kubeVersion != null then "--kube-version=${kubeVersion}" else ""} ${if version != null then "--version=${version}" else ""} ${if values != null then "-f ${valuesFile}" else ""} ${extraArgs} --namespace ${namespace} > $out/${name}.yaml
        runHook postBuild
      '';
      outputHashAlgo = "sha256";
      outputHashMode = "recursive";
      outputHash = helmHash;
    };
  };
}
